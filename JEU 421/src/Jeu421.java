public class Jeu421 {

    private static boolean peutRelancer = true;

    public static void main(String[] args) {

        initialiserEtJouer();

        int[][] figures = new int[100][3];
        int[] pointsFigures = new int[100];
        genererFiguresEtPoints(figures, pointsFigures);

        Ut.sautLigne();
        System.out.println("Bienvenu au jeu du 421");
        Ut.sautLigne();



        String[] joueurs = {"Joueur 1", "Joueur 2"};
        boolean[] estHumain = new boolean[joueurs.length];
        for (int i = 0; i < joueurs.length; i++) {
            int choix = -1;

            while (choix != 0 && choix != 1) {
                System.out.println("Le " + joueurs[i] + " est-il un humain ? (1 = Oui, 0 = Non)");
                choix = Ut.saisirEntier();
                if (choix != 0 && choix != 1) {
                    System.out.println("Choix invalide. Veuillez entrer 0 ou 1 ");
                }
            }
            estHumain[i] = (choix == 1);
        }

        int jetons = 0;
        Ut.sautLigne();
        while (jetons < 5){
            System.out.println("Saisir le nombre de jetons par joueurs (minimum 5) : " );
            jetons = Ut.saisirEntier();
            if (jetons < 5){
                System.out.println("Entrez un entier positive et supérieur ou égal à 5");
            }
        }




        int jetonJ1 = jetons;
        int jetonJ2 = jetons;
        int pointsJ1 = 0;
        int pointsJ2 = 0;


        int tour = 1;
        int premierJoueur = (Math.random() < 0.5) ? 0 : 1;

        while (jetonJ1 > 0 && jetonJ2 > 0) {
            System.out.println("_______________TOUR " + tour + "__________________");

            int[] scores = new int[joueurs.length];


            if (premierJoueur == 0) {
                pointsJ1 = jouerTour(joueurs[0], estHumain[0], figures, pointsFigures);
                pointsJ2 = jouerTour(joueurs[1], estHumain[1], figures, pointsFigures);
            }
            else {
                pointsJ2 = jouerTour(joueurs[1], estHumain[1], figures, pointsFigures);
                pointsJ1 = jouerTour(joueurs[0], estHumain[0], figures, pointsFigures);
            }

            int[] nouveauxJetons = gestionJetons(pointsJ1, pointsJ2, jetonJ1, jetonJ2);
            jetonJ1 = nouveauxJetons[0];
            jetonJ2 = nouveauxJetons[1];

            Ut.sautLigne();
            if (pointsJ1 < pointsJ2) {
                System.out.println("Le joueur 2 remporte le tour : " + pointsJ2 + " à " + pointsJ1 + " Pour le joueur 2");
            }
            if (pointsJ1 > pointsJ2) {
                System.out.println("Le joueur 1 remporte le tour : " + pointsJ1 + " à " + pointsJ2 + " Pour le joueur 1");
            }
            if (pointsJ1 == pointsJ2) {
                System.out.println("Egalité");
            }


            Ut.sautLigne();
            System.out.println("Jetons restant par joueur : ");
            System.out.println("Joueur 1 : " + jetonJ1);
            System.out.print("Joueur 2 : " + jetonJ2);
            Ut.sautLigne();

            premierJoueur = 1 - premierJoueur;
            tour++;
        }

        System.out.println("La partie est terminée !");
        if (jetonJ1 <= 0) {
            System.out.println("Le joueur 2 a gagné !");
        } else if (jetonJ2 <= 0) {
            System.out.println("Le joueur 1 a gagné !");
        }

    }

    public static int jouerTour(String nomJoueur, boolean estHumain, int[][] figures, int[] pointsFigures) {

        System.out.println("------------------------------------");
        System.out.println("C'est au tour du " + nomJoueur);

        int[] des = new int[3];
        boolean[] jet = {true, true, true};
        lancerDes(des, jet);
        afficherDes(des);

        if (peutRelancer = true){
            if (estHumain) {

                int relancer = -1;
                Ut.sautLigne();
                while (relancer != 0 && relancer != 1){
                    System.out.println("Voulez vous relancez des dès ? (1 pour OUI, 0 pour NON)");
                    relancer = Ut.saisirEntier();
                    if (relancer != 0 && relancer != 1){
                        System.out.println("Valeur invalide, veuillez entrer 0 ou 1");
                    }
                }
                if (relancer == 1){
                    for (int i = 0; i < 3; i++) {

                        int choix = -1;
                        while (choix != 0 && choix != 1) {
                            System.out.print("Relancer le dé " + (i + 1) + " ? (1 pour OUI, 0 pour NON) : ");
                            choix = Ut.saisirEntier();
                            if (choix != 0 && choix != 1) {
                                System.out.println("Choix invalide. Veuillez entrer 0 ou 1");
                            }
                        }

                        jet[i] = choix == 1;

                    }
                    lancerDes(des, jet);
                    afficherDes(des);
                }
                else if(relancer == 0){
                    peutRelancer = false;
                    System.out.println("Le joueur ne peut pas relancer");
                }
            }
            else {
                for (int i = 0; i < 3; i++) {
                    jet[i] = Math.random() > 0.5;
                }
                lancerDes(des, jet);
                afficherDes(des);
            }
        }
        else {
            System.out.println("Le joueur ne peut pas relancer ses dés");
        }


        int points = obtenirPoints(des, figures, pointsFigures);
        System.out.println(nomJoueur + " a obtenu " + points + " points !");
        return points;

    }

    public static void initialiserEtJouer() {

        int[][] figures = new int[16][0];
        int[] pointsFigures = new int[16];


        figures[0] = new int[]{3, 2, 1};
        figures[1] = new int[]{4, 3, 2};
        figures[2] = new int[]{5, 4, 3};
        figures[3] = new int[]{6, 5, 4};
        figures[4] = new int[]{2, 1, 1};
        figures[5] = new int[]{2, 2, 2};
        figures[6] = new int[]{3, 1, 1};
        figures[7] = new int[]{3, 3, 3};
        figures[8] = new int[]{4, 1, 1};
        figures[9] = new int[]{4, 4, 4};
        figures[10] = new int[]{5, 1, 1};
        figures[11] = new int[]{5, 5, 5};
        figures[12] = new int[]{6, 1, 1};
        figures[13] = new int[]{6, 6, 6};
        figures[14] = new int[]{1, 1, 1};
        figures[15] = new int[]{4, 2, 1};

        pointsFigures[0] = 2;
        pointsFigures[1] = 2;
        pointsFigures[2] = 2;
        pointsFigures[3] = 2;
        pointsFigures[4] = 2;
        pointsFigures[5] = 2;
        pointsFigures[6] = 3;
        pointsFigures[7] = 3;
        pointsFigures[8] = 4;
        pointsFigures[9] = 4;
        pointsFigures[10] = 5;
        pointsFigures[11] = 5;
        pointsFigures[12] = 6;
        pointsFigures[13] = 6;
        pointsFigures[14] = 7;
        pointsFigures[15] = 11;

    }

    public static int[] gestionJetons(int pointsJ1, int pointsJ2, int jetonJ1, int jetonJ2) {
        if (pointsJ1 > pointsJ2) {
            jetonJ1 -= pointsJ1;
            jetonJ2 += pointsJ1;
        }
        else if (pointsJ2 > pointsJ1) {
            jetonJ2 -= pointsJ2;
            jetonJ1 += pointsJ2;
        }

        if (jetonJ1 < 0){
            jetonJ1 = 0;
        }
        else if (jetonJ2 < 0){
            jetonJ2 = 0;
        }

        return new int[]{jetonJ1, jetonJ2};
    }



    //Gestion des dés et affichage des dés
    public static void lancerDes(int[] resultat, boolean[] jet) {
        for (int i = 0; i < resultat.length; i++) {

            if (jet[i]) {
                resultat[i] = (int) (Math.random() * 6) + 1;
            }
        }
        ordonnerResultatDec(resultat);
    }
    public static void afficherDes(int[] des) {
        System.out.print("Dés : ");
        for (int i = 0; i < des.length; i++) {
            System.out.print(des[i] + " ");
        }
        System.out.println();

    }
    public static void ordonnerResultatDec(int[] resultat) {
        for (int i = 0; i < resultat.length - 1; i++) {
            int maxIndex = i;

            for (int j = i + 1; j < resultat.length; j++) {
                if (resultat[j] > resultat[maxIndex]) {
                    maxIndex = j;
                }
            }
            echangerElementsTableau(resultat, i, maxIndex);
        }
    }
    public static void echangerElementsTableau(int[] tab, int indexA, int indexB) {
        int temp = tab[indexA];
        tab[indexA] = tab[indexB];
        tab[indexB] = temp;
    }



    //Figures et points
    public static void genererFiguresEtPoints(int[][] figures, int[] pointsFigures) {

        int[][] figuresSpeciales = new int[][]{
                {3,2,1}, {4,3,2}, {5,4,3}, {6,5,4}, {2,1,1}, {2,2,2}, {3,1,1}, {3,3,3},
                {4,1,1}, {4,4,4}, {5,1,1}, {5,5,5}, {6,1,1}, {6,6,6}, {1,1,1}, {4,2,1}
        };
        int[] pointFiguresSpeciales = new int[]{2, 2, 2, 2, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 11};

        int index = 0;

        for (int i = 0; i < figuresSpeciales.length; i++) {
            figures[index] = figuresSpeciales[i];
            pointsFigures[index] = pointFiguresSpeciales[i];
            index++;
        }

        for (int d1 = 6; d1 >= 1; d1--) {
            for (int d2 = d1; d2 >= 1; d2--) {
                for (int d3 = d2; d3 >= 1; d3--) {
                    int[] figureCommune = {d1, d2, d3};

                    if (!contientFigureTabFigures(figureCommune, figures)) {
                        figures[index] = figureCommune;
                        pointsFigures[index] = 1;
                        index++;
                    }
                }
            }
        }
    }
    public static boolean contientFigureTabFigures(int[] figure, int[][] figuresSpeciales) {
        for (int i = 0; i < figuresSpeciales.length; i++) {
            boolean match = true;
            for (int j = 0; j < figure.length; j++) {
                if (figuresSpeciales[i][j] != figure[j]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return true;
            }
        }
        return false;
    }
    public static int obtenirPoints(int[] figure, int[][] figures, int[] pointsFigures) {
        for (int i = 0; i < figures.length; i++) {

            if (figures[i][0] == figure[0] && figures[i][1] == figure[1] && figures[i][2] == figure[2]) {
                return pointsFigures[i];
            }
        }
        return 0;
    }



}